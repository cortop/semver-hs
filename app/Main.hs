module Main where

import Data.List
import Data.Maybe
import System.Environment   
import System.IO  
import Data.Typeable
import Text.Read

import Lib
import SemVer

main = do 
    args <- getArgs
    let maybeFilenameAndSemverChange = parseArgs args
    case maybeFilenameAndSemverChange of
        Nothing -> putStrLn "Wrong arguments"
        Just (filename, semVerChange) -> do
            -- Load file content
            content <- readFile filename
            -- Get current version
            let maybeCurrentSemVer = extractSemVer content
            case maybeCurrentSemVer of
                Nothing -> putStrLn "Impossible to parse version"
                Just currentSemVer -> do
                    let newSemVer = updateSemVer currentSemVer semVerChange
                    putStrLn (updateVersionInFileContent content newSemVer)
            
    -- Update version in text
    -- Write updated file



    -- putStrLn(show (fromString "1.2.6"))
    
    -- let (filename, semverChange) = (fromMaybe ("error", "error") (parseArgs args))
