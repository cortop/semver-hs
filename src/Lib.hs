module Lib
    (parseArgs,
     extractSemVer,
     updateVersionInFileContent
    ) where

import Data.Algorithms.KMP
import Data.Typeable
import System.IO
import Text.Read

import SemVer

parseArgs :: [String] -> Maybe (String, String)
parseArgs (filename : semverChange : _) = Just (filename, semverChange)
parseArgs _ = Nothing

extractSemVer :: String -> Maybe SemVerObj
extractSemVer fileContent = do
    let prefix = "    tag: "
    let firstPrefixMatchIdx = getPrefixFirstIdx prefix fileContent
    fromString (head (lines (drop (firstPrefixMatchIdx + (length prefix)) fileContent)))

updateVersionInFileContent :: String -> SemVerObj -> String
updateVersionInFileContent fileContent obj = do
    let prefix = "    tag: "
    let firstPrefixMatchIdx = getPrefixFirstIdx prefix fileContent
    let (firstPart, secondPart) = splitAt firstPrefixMatchIdx fileContent
    firstPart ++ prefix ++ (toString obj) ++ "\n" ++ unlines (tail (lines secondPart))

getPrefixFirstIdx :: String -> String -> Int
getPrefixFirstIdx prefix content = do
    let kmpTable = build prefix
    let result = match kmpTable content
    head result
