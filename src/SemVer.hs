module SemVer
    (SemVerObj(..),
     toString,
     fromString,
     updateSemVer
    ) where

import Data.List.Split
import Text.Read

data SemVerObj = SemVerObj Integer Integer Integer deriving (Show)

toString :: SemVerObj -> String
toString (SemVerObj major minor patch) =
   (show major) ++ "." ++ (show minor) ++ "." ++ (show patch)

fromString :: String -> Maybe SemVerObj
fromString source = fromStringList (splitOn "." source)

fromStringList :: [String] -> Maybe SemVerObj
fromStringList (majorS : minorS : patchS : _) = do
    let maybeMajor = (readMaybe majorS :: Maybe Integer)
    let maybeMinor = (readMaybe minorS :: Maybe Integer)
    let maybePatch = (readMaybe patchS :: Maybe Integer)

    maybeMajor >>= \major -> (
        maybeMinor >>= \minor -> (
            maybePatch >>= \patch -> (
                return (SemVerObj major minor patch))))

updateSemVer :: SemVerObj -> String -> SemVerObj
updateSemVer (SemVerObj major minor patch) "MAJOR" = SemVerObj (major + 1) 0 0
updateSemVer (SemVerObj major minor patch) "MINOR" = SemVerObj major (minor + 1) 0
updateSemVer (SemVerObj major minor patch) "PATCH" = SemVerObj major minor (patch + 1)
updateSemVer (SemVerObj major minor patch) _ = SemVerObj major minor patch
